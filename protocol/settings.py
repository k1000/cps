from django.conf import settings

PROTOCOL_FROM  = getattr(settings, 'PROTOCOL_FROM', 'SSF Protocol System <protocol@shangshunginstitute.org>')
PROTOCOL_CATEGORY_NOT_FOUND  = getattr(settings, 'PROTOCOL_CATEGORY_NOT_FOUND', u"Protocol category has not been found.")
PROTOCOL_CATEGORY_NOT_FOUND_MSG  = getattr(settings, 'PROTOCOL_CATEGORY_NOT_FOUND_MSG', u"""Protocol category has not been found in the email subject. 
Please prefix protocol category to the email subject. like: '/ORG/2/2/' """)
PROTOCOL_WRONG_CATEGORY  = getattr(settings, 'PROTOCOL_WRONG_CATEGORY', u"Protocol category '%s' not found.")
PROTOCOL_WRONG_CATEGORY_MSG  = getattr(settings, 'PROTOCOL_WRONG_CATEGORY_MSG', u"""Category '%s' does not correspond to any registered protocol category. 
Please make sure you got it spelled correctly""")
PROTOCOL_CAT_NOT_ALLOWED  = getattr(settings, 'PROTOCOL_CAT_NOT_ALLOWED', "Protocol category '%s' does not allows posting")
PROTOCOL_CAT_NOT_ALLOWED_MSG  = getattr(settings, 'PROTOCOL_CAT_NOT_ALLOWED_MSG', "Protocol category '%s' does not allows posting")
PROTOCOL_REF_NOT_FOUND  = getattr(settings, 'PROTOCOL_REF_NOT_FOUND', u"Protocol '%s' not found.")
PROTOCOL_REF_NOT_FOUND_MSG  = getattr(settings, 'PROTOCOL_REF_NOT_FOUND_MSG', u"""Protocol reference %s has not been found in the email subject. 
Please check its spelling""")
PROTOCOL_ALLOWED_DOMAINS  = getattr(settings, 'PROTOCOL_ALLOWED_DOMAINS', ["shangshunginstitute.org", "shangshungfoundation.org"])
PROTOCOL_SENDER_NOT_ALLOWED  = getattr(settings, 'PROTOCOL_SENDER_NOT_ALLOWED', "'%s' sender not allowed")
PROTOCOL_DOMAIN_NOT_ALLOWED_MSG  = getattr(settings, 'PROTOCOL_DOMAIN_NOT_ALLOWED_MSG', "'%s' sender domain not allowed")

PROTOCOL_REF_FOUND  = getattr(settings, 'PROTOCOL_REF_FOUND', "Message '%s' has been added to existing protocol")
PROTOCOL_REF_FOUND_MSG  = getattr(settings, 'PROTOCOL_REF_FOUND_MSG', """Your message 
'%s' 
has been added to the existing corespondence protocol ref: '%s'
at %s""")
PROTOCOL_REF_CREATED  = getattr(settings, 'PROTOCOL_REF_CREATED', "Email '%s' has been added to the SSF Protocol")
PROTOCOL_REF_CREATED_MSG  = getattr(settings, 'PROTOCOL_REF_CREATED_MSG', """This email has been added successfully to the SSF Correspondence Protocol with ref: '%s'
<br>
at %s""")