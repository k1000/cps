from django.conf.urls import url
from .views import get_emails, list_categories, index, file_view
from .views import browse, search, correspondence, new_correspondence

urlpatterns = [
    url(r'^get-emails/$',
        get_emails, name="get_emails"),
    url(r'^json/$',
        list_categories, name="protocol_categories_json"),
    url(r"^search/$",
        search, name="protocol_search"),
    url(r"^(?P<corr_ref>[0-9a-zA-Z_\/\-]+\/\d\d\d\d_[\d]+)$",
        correspondence, name="protocol_correspondence"),
    url(r"^new/$",
        new_correspondence, name="protocol_new_correspondence"),
    url(r"^attachment/(?P<doc_id>\d+)/$",
        file_view, name="protocol_attachment"),
    url(r"^(?P<protocol_cat>[0-9a-zA-Z_\/\-]+\/)$",
        browse, name="protocol_browse"),
    url(r'^$',
        index, name="protocol_categories"),
]
