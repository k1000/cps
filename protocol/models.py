from __future__ import unicode_literals
import os
import email
from datetime import date
from django.db import models
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.conf import settings

# class Scheme(models.Model):
#     name = models.CharField(max_length=255)
#     description = models.CharField(max_length=255)
#     created_at = models.DateTimeField(auto_now_add=True)

#     def __str__(self):
#         return unicode(self.name)

#     class Meta:
#         get_latest_by = 'created_at'

def create_directory(root, path):
    dir_path = os.path.join(root, path)
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)


class Category(models.Model):
    #scheme = models.ForeignKey(Scheme)
    parent = models.ForeignKey('Category',
        verbose_name="belongs to",
        blank=True, null=True,
        related_name="subcategories")

    name = models.CharField(max_length=255)
    # in admin: prepopulated_fields = {"slug": ("name",)}
    index = models.PositiveIntegerField(null=True, blank=True)
    slug = models.SlugField("abbreviation", max_length=255)
    reference = models.CharField(max_length=255, unique=True)
    reference_verbose = models.CharField("directory", max_length=255, unique=True)
    allows_storage = models.BooleanField()
    description = models.TextField(
        blank=True, null=True)

    def make_path(self):
        if self.parent:
            return "%s%s/" % (self.parent.reference,  self.index)
        else:
            return "%s/" % self.slug

    def make_human_path(self):
        slug = self.slug if not self.index else "%s_%s" % (self.index, self.slug)
        return "%s%s/" % ( self.parent.reference_verbose if self.parent else "",  slug)

    @classmethod
    def storage_categories(cls):
        return cls.objects.filter(allows_storage=True)

    def path(self):
        return self.reference_verbose

    def __str__(self):
        return self.reference_verbose

    def get_parents(self):
        # TODO cacheit on root key
        def get_parent(page, parents):
            parent = page.parent
            if parent:
                parents.append(parent)
                return get_parent(parent, parents)
            else:
                return None, parents

        return get_parent(self, [])[1][::-1]

    def get_parents_from_url(self, url=None):
        return (url or self.url).split("/")[1:-2]

    def save(self, *args, **kwargs):
        self.reference = self.make_path()
        self.reference_verbose = self.make_human_path()
        # import ipdb; ipdb.set_trace()
        storage_root = os.path.join(settings.MEDIA_ROOT, 'protocol')
        create_directory(storage_root, self.reference_verbose)
        super(Category, self).save(*args, **kwargs)

    def clean(self):
        if not self.index and self.parent:
            raise ValidationError('Subcategories should have index number')

        if self.index and not self.parent:
            raise ValidationError('Top categories don\'t have index number')

    class Meta():
        verbose_name_plural = "Categories"
        ordering = ("reference_verbose",)
        unique_together = (("index", "parent"),)


class StorageLocation(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()

    def __str__(self):
        return self.name


def storage_categories():
    #  https://djangosnippets.org/snippets/1968/
    cats = Category.storage_categories().values_list("id", flat=True)
    return {"id__in": cats } 


class Communication(models.Model):
    is_cancelled = models.BooleanField(default=False)

    protocol = models.CharField(max_length=255, db_index=True, unique=True)
    protocol_verbose = models.CharField("Directory", max_length=255, db_index=True, unique=True)

    index = models.CharField(max_length=255, null=True, blank=True)
    category = models.ForeignKey(
        Category, limit_choices_to=storage_categories)
    subject = models.CharField(max_length=255)
    frm = models.CharField("from", max_length=255)
    to = models.CharField(max_length=255)
    body = models.TextField(blank=True, null=True)

    submitted = models.DateTimeField(auto_now_add=True)
    is_incoming = models.BooleanField()

    storage = models.ForeignKey(StorageLocation,
        null=True, blank=True,
        help_text="Physical storage location")
    is_digital = models.BooleanField()
    metadata = models.TextField(blank=True, null=True)

    class Meta:
        get_latest_by = 'submitted'
        ordering = ("-submitted",)
        verbose_name="Correspondence"
        verbose_name_plural="Correspondences"

    def get_last_id(self):
        current_year = date.today().year
        return current_year, Communication.objects.filter(submitted__year=current_year).count()

    def create_id(self):
        current_year, latest_id = self.get_last_id()
        return current_year, latest_id + 1

    def save(self, *args, **kwargs):
        if not self.id:
            # super(Communication, self).save(*args, **kwargs)
            self.index = "%s_%s" % self.create_id()
        self.protocol = "%s%s" % (self.category.reference, self.index)
        self.protocol_verbose = "%s%s" % (self.category.reference_verbose, self.index)

        super(Communication, self).save(*args, **kwargs)

    def path(self):
        return self.protocol_verbose[1:]
        
    def clean(self):
        # if not self.is_digital and not self.storage:
        #     raise ValidationError('Storage location must be set for physical correspondence')

        if self.is_digital and self.storage:
            raise ValidationError('Storage location does not apply for digital correspondence only for physical')

    @property
    def storage_dir(self):
        return "protocol%s" % self.protocol_verbose

    def frm_address(self):
        return email.utils.getaddresses([self.frm])[0]
        
    def get_text(self):
        return self.document_set.filter(body__isnull=False)

    def get_attachments(self):
        return self.document_set.filter(attachment__isnull="")

    def get_absolute_url(self):
        #import ipdb; ipdb.set_trace()
        return reverse(
            "protocol_correspondence", kwargs={'corr_ref': self.path()})

    def __str__(self):
        return "%s %s" % (self.protocol, self.subject)


def get_storage_location(instance, filename):
        return "%s-%s" % (instance.communication.storage_dir, filename)


class Document(models.Model):
    communication = models.ForeignKey(Communication)
    message_id = models.CharField(max_length=255, blank=True, null=True)
    submitted_at = models.DateTimeField(auto_now_add=True)
    attachment = models.FileField(
        upload_to = get_storage_location,
        blank=True, null=True)
    body = models.TextField(blank=True, null=True)
    metadata = models.TextField(blank=True, null=True)
    
    def __str__(self):
        return self.communication.protocol

    def filename(self):
        return os.path.basename(self.attachment.name)

    def clean(self):
        if not self.attachment and not self.body:
            raise ValidationError('File atachment or "body" of the message must be set')

    def save(self, *args, **kwargs):
        # TODO extract and save metadata as JSON

        super(Document, self).save(*args, **kwargs)


