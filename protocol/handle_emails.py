import json
import email
import imaplib
import re
import os
from io import BytesIO

from email.mime.message import MIMEMessage

from django.core.files import File
from django.core.mail import EmailMultiAlternatives
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.template.loader import render_to_string

from .models import Category, Communication, Document
from .settings import *


def notify(sbj, msg, to, original):
    new = EmailMultiAlternatives(
        "Re: "+ original["Subject"].replace("\r\n", " "),
         msg, 
         PROTOCOL_FROM, # from
         to, # to
         headers = {'Reply-To': original["From"],
                    "In-Reply-To": original["Message-ID"],
                    "References": original["Message-ID"]})

    email_html = render_to_string('protocol/msg.html', {'sbj': sbj, 'msg': msg})
    new.attach_alternative(email_html, "text/html")
    # new.attach( MIMEMessage(original) ) # attach original message
    try:
        new.send()
    except smtplib.SMTPDataError:
        import ipdb; ipdb.set_trace()


def new_email(correspondence):
    msg_sbj = "%s %s" % (correspondence.protocol, correspondence.subject)
    msg = PROTOCOL_REF_CREATED_MSG % (
        correspondence.protocol,
        correspondence.get_absolute_url())
    body = """%s

-----------------------------------
%s""" % (correspondence.body, msg)
    #
    new = EmailMultiAlternatives(
        correspondence.subject,
        body, 
        correspondence.frm, # from
        [correspondence.frm, correspondence.to], # to
        headers = {'Reply-To': correspondence.frm}
    )
    import ipdb; ipdb.set_trace()
    email_suffix = render_to_string('protocol/msg.html', 
        {'sbj': correspondence.subject, 'msg': msg})
    email_html = "%s<br><br>%s" % (correspondence.body, email_suffix)
    new.attach_alternative(email_html, "text/html")
    for att in correspondence.get_attachments():
        new.attach_file(att.attachment.path)
    new.send()


def is_allowed_sender(frm):
    # checks is email domain is on whitelist
    from_pattern = re.compile(r'^.+@([\w\.]+).*$')
    domain = from_pattern.findall(frm)[0]
    return domain in PROTOCOL_ALLOWED_DOMAINS

def get_unseen_emails():
    # https://yuji.wordpress.com/2011/06/22/python-imaplib-imap-example-with-gmail/
    output = []
    mailbox = imaplib.IMAP4_SSL('imap.gmail.com','993')
    mailbox.login(settings.PROTOCOL_EMAIL, settings.PROTOCOL_EMAIL_PASS)
    mailbox.select()
    result, data = mailbox.search(None, 'UnSeen')  #'UnSeen') # "ALL"
    ids = data[0] # data is a list.
    id_list = ids.split() # ids is a space separated string
    
    for e in id_list:
        result, data = mailbox.fetch(e, "(RFC822)") # fetch the email body (RFC822) for the given ID
        email_message = email.message_from_bytes(data[0][1])
        # raw_data = str(data[0][1]).replace("\\r\\n", "\r\n")
        # email_message = email.message_from_string(raw_data)
        success, communication = process_request_msg(mailbox, email_message)
        output.append([success, "%s" % communication.protocol if success else communication])
    mailbox.close()
    mailbox.logout()
    return output


def process_request_msg(mailbox, request_msg):
    sbj = msg = u""
    success = False
    communication = None
    category = None
    protocol_path = ""
    subject_text = ""
    subject = request_msg['Subject']
    frm = request_msg['From']
    all_recipients = email.utils.getaddresses(request_msg.get_all('to', []))
    recipients_list =[]
    to = []
    for n, e in all_recipients:
        email_to = "%s <%s>" % (n, e)
        if e == settings.PROTOCOL_EMAIL:
            continue
        if email_to == frm:
            continue
        recipients_list.append((n, e))
        to.append(email_to)
    notify_emails = [frm]

    if not is_allowed_sender(frm):
        return False, PROTOCOL_SENDER_NOT_ALLOWED % frm

    # we check for protocol category reference
    try:
        category_ref, subject_text = re.search(
            r"^ *(?P<protocol_cat>[0-9a-zA-Z\/]+\/) (?P<subject>.*)$", subject).groups()
    except:
        sbj = PROTOCOL_CATEGORY_NOT_FOUND
        msg = PROTOCOL_CATEGORY_NOT_FOUND_MSG
    else:
        # we check for protocol if category exist
        try:
            category = Category.objects.get(reference=category_ref)
        except Category.DoesNotExist:
            sbj = PROTOCOL_WRONG_CATEGORY % category_ref
            msg = PROTOCOL_WRONG_CATEGORY_MSG % category_ref
        else:
            if category.allows_storage:
                metadata=dict(request_msg.items())
                #del(metadata["b'Delivered-To"])  #HACK 
                communication = Communication(
                    category=category,
                    frm=frm,
                    to=", ".join(to),
                    subject=subject_text,
                    is_incoming=True,
                    is_digital=True,
                    metadata=json.dumps(metadata)
                )
                communication.save()
                success = True
                sbj = PROTOCOL_REF_CREATED % subject_text
                msg = PROTOCOL_REF_CREATED_MSG % (
                    subject_text, communication.protocol, communication.protocol)
                notify_emails = notify_emails + to
            else:
                sbj = PROTOCOL_CAT_NOT_ALLOWED % category_ref
                msg = PROTOCOL_CAT_NOT_ALLOWED_MSG % category_ref
            
    # TODO protocol forwarded emails as incoming
    if not communication:
        # we check for particular protocol reference
        try:
            protocol_ref, subject_text = re.search(
                r"^ *(?P<protocol_cat>[0-9a-zA-Z\/]+/)(?P<cor_ref>\d\d\d\d-\d+) (?P<subject>.*)$", subject).groups()
        except UnboundLocalError:
            sbj = PROTOCOL_WRONG_CATEGORY % ""
            msg = PROTOCOL_WRONG_CATEGORY_MSG % ""
        except AttributeError:
            sbj = PROTOCOL_WRONG_CATEGORY % ""
            msg = PROTOCOL_WRONG_CATEGORY_MSG % ""
        else:
            # we check if particular protocol reference is alredy present
            try:
                communication = Communication.objects.get(protocol=protocol_ref)
            except:
                sbj = PROTOCOL_REF_NOT_FOUND % protocol_ref
                msg = PROTOCOL_REF_NOT_FOUND_MSG % protocol_ref
            else:
                success = True
                sbj = PROTOCOL_REF_FOUND % subject_text
                msg = PROTOCOL_REF_FOUND_MSG % (
                    subject_text, communication.protocol, communication.protocol)
                notify_emails = notify_emails + to
    
    if success:
        documents = process_email(communication, request_msg)

    notify(sbj, msg, notify_emails, request_msg)
    return success, communication if success else sbj


def process_email(communication, m):
    docs = []
    texts = []
    for part in m.walk():
        ctype = part.get_content_type()
        cdispo = part.get_content_disposition()   #str(part.get('Content-Disposition'))
        filename=part.get_filename()
        metadata = dict(part.items())
        if ctype in ['text/plain', 'text/html'] and not filename:
            # we save to Comunication first found 'text/html'
            if ctype == 'text/html' and not communication.body:
                # https://github.com/divio/djangocms-text-ckeditor/blob/master/djangocms_text_ckeditor/sanitizer.py
                # TODO sanitize
                communication.body = part.get_payload()
                communication.save()
            else:
                doc = Document(
                    communication=communication,
                    message_id=part["Message-ID"],
                    body=part.get_payload(),
                    metadata=json.dumps(metadata)
                )
                doc.save()
                docs.append(doc)
        if filename:
            doc = Document(
                communication=communication,
                metadata=json.dumps(metadata)
            )
            attachment = BytesIO()
            attachment.write(part.get_payload(decode=True))
            doc.attachment.save(filename, File(attachment), save=True)
            doc.save()
            docs.append(doc)
    return docs