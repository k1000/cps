import json

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.forms import ModelForm
from django.forms import inlineformset_factory
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect

from .models import Category, Communication, Document
from .handle_emails import get_unseen_emails, new_email


def get_emails(request):
    output = get_unseen_emails()
    return JsonResponse(output, safe=False)

def list_categories(request):
    categories = Category.objects.all()
    cats = [[c.reference, c.name] for c in categories]
    return JsonRespone(cats)

@staff_member_required
def index(request):
    categories = Category.objects.filter(allows_storage=True)
    latest = Communication.objects.filter()[:15]
    return render(request, 'protocol/index.html', 
        dict(cats=categories, latest=latest))

@staff_member_required
def browse(request, protocol_cat):
    protocol_cat = '%s' % protocol_cat
    category = get_object_or_404(Category, reference_verbose=protocol_cat)
    return render(request, 'protocol/category.html', dict(category=category))

@staff_member_required
def correspondence(request, corr_ref):
    protocol = '/%s' % corr_ref
    corr = get_object_or_404(Communication, protocol_verbose=protocol)
    return render(request, 'protocol/correspondence.html', dict(corr=corr))

class CorrespondenceForm(ModelForm):
    class Meta:
        model = Communication
        fields = ['category', 'subject', 'frm', 'to', 'body']

@staff_member_required
def new_correspondence(request):
    correspondence = None
    form = CorrespondenceForm(request.POST or None, 
        initial={'frm': request.user.email, 'is_digital':True, 'is_incoming':False})
    #form.data['is_digital'] = True #HACK
    AttachemntInlineFormSet = inlineformset_factory(
        Communication, Document, fields=('attachment',))
    #import ipdb; ipdb.set_trace()
    if request.method == 'POST':
        # formset = AttachemntInlineFormSet(request.POST, request.FILES)
        if form.is_valid():
            correspondence = form.save(commit=False)
            correspondence.is_digital = True
            correspondence.is_incoming = False
            correspondence.save()

        formset = AttachemntInlineFormSet(
                request.POST, request.FILES, instance=correspondence)
        if correspondence and formset.is_valid():
            # TODO db trasmission
            formset.save()
            new_email(correspondence)
            return redirect(correspondence)
    else:
        formset = AttachemntInlineFormSet()

    return render(request, 'protocol/new-correspondence.html', 
        dict(form=form, formset=formset))

@staff_member_required
def new_document(request, protocol_cat):
    correspondence = None
    protocol_cat = '/%s' % protocol_cat
    category = get_object_or_404(Category, reference_verbose=protocol_cat)
    form = CorrespondenceForm(request.POST or None, 
        initial={'frm': request.user.email, 'category': category, 'is_digital':False, 'is_incoming':True})
    #form.data['is_digital'] = True #HACK
    AttachemntInlineFormSet = inlineformset_factory(
        Communication, Document, fields=('attachment',))
    #import ipdb; ipdb.set_trace()
    if request.method == 'POST':
        # formset = AttachemntInlineFormSet(request.POST, request.FILES)
        if form.is_valid():
            correspondence = form.save(commit=False)
            correspondence.is_digital = True
            correspondence.is_incoming = False
            correspondence.save()

        formset = AttachemntInlineFormSet(
                request.POST, request.FILES, instance=correspondence)
        if correspondence and formset.is_valid():
            # TODO db trasmission
            formset.save()
            new_email(correspondence)
            return redirect(correspondence)
    else:
        formset = AttachemntInlineFormSet()

    return render(request, 'protocol/new-correspondence.html', 
        dict(form=form, formset=formset, category=category))

@staff_member_required
def search(request):
    if request.GET:
        search_str = request.GET.get('search')
        results = Communication.objects.filter(subject__icontains=search_str)
    return render(request, 'protocol/search.html', dict(results=results))


@staff_member_required
def file_view(request, doc_id):
    # https://wellfire.co/learn/nginx-django-x-accel-redirects/
    # https://stackoverflow.com/questions/1156246/having-django-serve-downloadable-files#answer-1158750
    document = get_object_or_404(Document, id=doc_id)
    response = HttpResponse()
    response.content = document.attachment.read()
    response['Content-Disposition'] = 'attachment; filename={0}'.format(
            document.filename())
    return response